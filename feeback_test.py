import time

from motor_control import lac
from data_collection import readADC

a, b = lac.setupMultiple()
adc = readADC.ADC()

b.set_accuracy(4)
while True:
    # retract actuator to start point
    b.set_speed(400)
    b.set_position(100)
    time.sleep(10)

    # start moving to endpoint
    b.set_speed(200)
    b.set_position(900)
    cur_pos = b.get_feedback()

    while cur_pos < 895:
        # stop the motor when it puts pressure on the resistor
        while adc.channel.value > 2000:
            b.set_position(cur_pos)
            print('stopped')
            time.sleep(0.25)
        
        # reset position to end, and report current position
        b.set_position(900)
        time.sleep(0.1)
        cur_pos = b.get_feedback()
        print(cur_pos)
