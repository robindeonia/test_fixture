## Introduction
This repo contains code for the high-level interface to the test fixture, using feedback from a pressure sensitive resistor to control actuators.

## Setup
Plese folow the setup instructions in these repos for both hardware and dependencies:

motor_control
data_collection

Using a [virtual environment](https://docs.python.org/3/tutorial/venv.html) can help keep things clean and bundled.

## Usage
Currently the feedback_test.py script tests a single actuator and pressure sensitive resistor.  It moves the actuator in and out repeatedly, stopping and starting as pressure is applied to the pressure sensitive resistor. 

